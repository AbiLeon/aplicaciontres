package com.example.aplicaciontres.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.aplicaciontres.R
import kotlinx.android.synthetic.main.activity_magnament.*


/**
 * @creationDate         05,July,2020
 * @modificationDate     05,July,2020
 * @author               José Angel Fierro Hernández
 * @company              Banco Azteca
 * @version              1.0
 *
 */
class MagnamentActivity :AppCompatActivity(), View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_magnament)
        btn_magnament_autotext.setOnClickListener(this)
        btn_magnament_main.setOnClickListener(this)
        btn_magnament_picker_date.setOnClickListener(this)
        btn_magnament_picker_time.setOnClickListener(this)
        btn_magnament_spinner.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            btn_magnament_autotext.id ->{
                startActivity(Intent(this, AutoTextActivity::class.java))
            }
            btn_magnament_main.id->{
                startActivity(Intent(this, MainActivity::class.java))
            }
            btn_magnament_picker_date.id ->{
                startActivity(Intent(this, PickerDateActivity::class.java))
            }
            btn_magnament_picker_time.id->{
                startActivity(Intent(this, PickerTimeActivity::class.java))
            }
            btn_magnament_spinner.id->{
                startActivity(Intent(this, SpinnerActivity::class.java))
            }

        }
    }
}