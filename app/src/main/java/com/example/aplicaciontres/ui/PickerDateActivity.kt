package com.example.aplicaciontres.ui

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.aplicaciontres.R
import com.example.aplicaciontres.generic.DatePickerFragment
import kotlinx.android.synthetic.main.activity_picker_date.*

class PickerDateActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_date)
        btn_picker_date.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            btn_picker_date.id ->{
                showPickerDateDialog()
            }
        }
    }

    fun Int.twoDigits() =
        if (this <= 9) "0$this" else this.toString()

    private fun showPickerDateDialog() {

        val datePickerFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { view, year, month, dayOfMonth ->
            val dayStr = dayOfMonth.twoDigits()
            val monthStr = (month+1).twoDigits()
            et_picker_date.setText("${year}-${monthStr}-${dayStr}")
        })

        datePickerFragment.show(supportFragmentManager, "datePicker")

    }
}
