package com.example.aplicaciontres.ui

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.aplicaciontres.R
import com.example.aplicaciontres.generic.TimePickerFragment
import kotlinx.android.synthetic.main.activity_picker_time.*

class PickerTimeActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_time)
        btn_piker_time.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            btn_piker_time.id ->{
                showPickerTimeDialog()
            }
        }
    }

    fun Int.twoDigits() =
        if (this <= 9) "0$this" else this.toString()

    private fun showPickerTimeDialog() {

        val timerPickerFragment =
            TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                val hourStr = hourOfDay.twoDigits()
                val minStr = minute.twoDigits()
                et_picker_time.setText("${hourStr}:${minStr}")
            })
        timerPickerFragment.show(supportFragmentManager, "timePicker")
    }

}
