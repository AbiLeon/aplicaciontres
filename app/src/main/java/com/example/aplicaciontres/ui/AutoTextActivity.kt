package com.example.aplicaciontres.ui

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.aplicaciontres.R
import kotlinx.android.synthetic.main.activity_auto_text.*


class AutoTextActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_text)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item
        )

        autoCompleteTextView2.threshold = 2
        autoCompleteTextView2.setAdapter(adapter)
        autoCompleteTextView2.setOnItemClickListener { parent, view, position, id ->

            val reference = "Posicion ${position} Alcadia seleccionada : ${parent!!.getItemAtPosition(position)}"
            showToast(referencia = reference)

        }
    }

    private fun showToast(referencia: String) {
        Toast.makeText(this, referencia, Toast.LENGTH_SHORT).show()
    }
}
